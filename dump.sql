-- MySQL dump 10.13  Distrib 5.5.34, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: bookcase_db
-- ------------------------------------------------------
-- Server version	5.5.34-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `author`
--

DROP TABLE IF EXISTS `author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author`
--

LOCK TABLES `author` WRITE;
/*!40000 ALTER TABLE `author` DISABLE KEYS */;
INSERT INTO `author` VALUES (3,'Daniel Defoe'),(5,'Henry Fielding'),(9,'Jane Austen'),(2,'John Bunyan'),(4,'Jonathan Swift'),(7,'Laurence Sterne'),(10,'Mary Shelley'),(1,'Miguel De Cervantes'),(8,'Pierre Choderlos De Laclos'),(6,'Samuel Richardson');
/*!40000 ALTER TABLE `author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `genre_id` int(11) DEFAULT NULL,
  `cover` varchar(256) DEFAULT NULL,
  `search_keywords` text COMMENT 'Contains all searchable information imploded by comma',
  PRIMARY KEY (`id`),
  KEY `idx_title` (`title`),
  FULLTEXT KEY `search_keywords_idx` (`search_keywords`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,'Don Quixote',1,7,'1.jpg','Don Quixote,Miguel De Cervantes,Fairytale,Chapter 1,Chapter 2,Chapter 3,Chapter 4,Chapter 5,don quixote,gentleman,la mancha,first sally,droll way,knight,inn events,narrative,mishap'),(2,'Pilgrim\'s Progress',2,13,'2.jpg','Pilgrim\'s Progress,John Bunyan,Fiction,City of Destruction,Slough of Despond,Village of Morality,Wicket Gate,Interpreter\'s House,city symbol,corruption,sins,hope,salvation,obstacles,Christian\'s course,pilgrimage,hunger,pain,death,grave person, celestial city,interpreter, holy spirit'),(3,'Robinson Crusoe',3,13,'3.jpg','Robinson Crusoe,Daniel Defoe,Fiction'),(4,'Gulliver\'s Travels',4,3,NULL,'Gulliver\'s Travels,Jonathan Swift,Satire'),(5,'Tom Jones',5,1,NULL,'Tom Jones,Henry Fielding,Drama'),(6,'Clarissa',6,2,NULL,'Clarissa,Samuel Richardson,Romance'),(7,'Tristram Shandy',7,3,NULL,'Tristram ShandyLaurence Sterne,Comedy'),(8,'Dangerous Liaisons',8,4,NULL,'Dangerous Liaisons,Pierre Choderlos De Laclos,Tragedy'),(9,'Emma',9,5,NULL,'Emma,Jane Austen,Comedy'),(10,'Frankenstein',10,9,NULL,'Frankenstein,Mary Shelley,Horror');
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chapter`
--

DROP TABLE IF EXISTS `chapter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chapter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(512) DEFAULT NULL,
  `keywords` text COMMENT 'Comma concatenating list of keywords/phrases',
  `book_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chapter`
--

LOCK TABLES `chapter` WRITE;
/*!40000 ALTER TABLE `chapter` DISABLE KEYS */;
INSERT INTO `chapter` VALUES (1,'Chapter 1','don quixote,gentleman,la mancha',1),(2,'Chapter 2','first sally',1),(3,'Chapter 3','droll way,knight',1),(4,'Chapter 4','inn events',1),(5,'Chapter 5','narrative,mishap',1),(6,'City of Destruction','city symbol,corruption,sins,hope,salvation',2),(7,'Slough of Despond','obstacles,Christian\'s course',2),(8,'Village of Morality','pilgrimage,hunger,pain,death',2),(9,'Wicket Gate','grave person, celestial city',2),(10,'Interpreter\'s House','interpreter, holy spirit',2);
/*!40000 ALTER TABLE `chapter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genre`
--

DROP TABLE IF EXISTS `genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(45) DEFAULT NULL,
  `color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_label` (`label`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genre`
--

LOCK TABLES `genre` WRITE;
/*!40000 ALTER TABLE `genre` DISABLE KEYS */;
INSERT INTO `genre` VALUES (1,'Drama','#0096fd'),(2,'Romance','#dcedcf'),(3,'Satire','#5ca028'),(4,'Tragedy','#f4da70'),(5,'Comedy','#f75b68'),(6,'Tragicomedy','#336666'),(7,'Fairytale','#646464'),(8,'Fantasy','#cc3333'),(9,'Horror','#990000'),(10,'Mystery','#808080'),(11,'Mythology','#800080'),(12,'Biography','#ffa500'),(13,'Fiction','#ffecce'),(14,'',NULL);
/*!40000 ALTER TABLE `genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_migration`
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_migration`
--

LOCK TABLES `tbl_migration` WRITE;
/*!40000 ALTER TABLE `tbl_migration` DISABLE KEYS */;
INSERT INTO `tbl_migration` VALUES ('m000000_000000_base',1399109414),('m140503_093523_setup_db',1399146378);
/*!40000 ALTER TABLE `tbl_migration` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-05-05  9:21:41
