<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the homepage
     */
    public function actionIndex($order = null, $search = null)
    {
        $this->render('index');
    }

    public function actionBooks($order = null, $query = null)
    {
        $attrs = [];
        if (isset($order)) {
            $attrs['order'] = $order;
        }
        if (isset($query)) {
            $attrs['query'] = $query;
        }

        // Fetch book collection
        $books = Book::search($attrs);

        // Prepare the book collection
        $bookCollection = [];
        foreach($books as $book)
        {
            // Book fields
            $map = $book->apiAttributes();
            // Author fields
            $map = CMap::mergeArray($map, [
                'author' => $book->author->apiAttributes()
            ]);
            // Genre fields
            $map = CMap::mergeArray($map, [
                'genre' => $book->genre->apiAttributes()
            ]);
            // Chapters fields
            $chapters = [];
            foreach($book->chapters as $chapter) {
                $chapters[] = $chapter->apiAttributes();
            }
            $map = CMap::mergeArray($map, [
                'chapters' => $chapters
            ]);
            // Push the book to the collection
            $bookCollection[] = $map;
        }

        $this->renderJSON($bookCollection);
    }
}
