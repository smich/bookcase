<?php

/**
 * Base controller class
 * All controller classes extend this one
 */
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/main';

    /**
     * Render a JSON response with correct Content-type header.
     *
     * @param mixed $data the data to render as JSON. Usually an array or object.
     * @param bool $endApplication whether to end the application afterwards. Defaults to false.
     */
    public function renderJSON($data, $endApplication = true)
    {
        $this->layout = false;
        header('Content-type: application/json');
        echo CJSON::encode($data);
        if ($endApplication) {
            $this->afterAction($this->action);
            Yii::app()->end();
        }
    }

}