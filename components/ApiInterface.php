<?php
/**
 * Interface for the models exposed to the API.
 */
interface ApiInterface
{
    /**
     * @return [] The list of attributes exposable to the API
     */
    public function apiAttributes();
}