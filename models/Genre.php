<?php

/**
 * Model class for table "genre"
 *
 * 'genre' fields:
 * @property integer $id
 * @property string $label
 * @property string $color
 */
class Genre extends CActiveRecord implements ApiInterface
{
    /**
     * @return Book the static model class
     */
    public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'genre';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('label', 'length', 'max'=>45),
			array('color', 'length', 'max'=>7),
			array('id, label, color', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'label' => 'Label',
			'color' => 'Color',
		);
	}

	/**
     * @return array The fields exposed to the API
     */
    public function apiAttributes()
    {
        return [
            'id' => $this->id,
            'label' => $this->label,
            'color' => $this->color,
        ];
    }
}