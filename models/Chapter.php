<?php

/**
 * This is the model class for table "chapter".
 *
 * 'chapter' fields:
 * @property integer $id
 * @property string $title
 * @property string $keywords
 * @property integer $book_id
 */
class Chapter extends CActiveRecord implements ApiInterface
{
    /**
     * @return Book the static model class
     */
    public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'chapter';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('book_id', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>512),
			array('keywords', 'safe'),
			array('id, title, keywords, book_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'keywords' => 'Keywords',
			'book_id' => 'Book',
		);
	}

	/**
     * @return array The fields exposed to the API
     */
    public function apiAttributes()
    {
        return [
            'title' => $this->title,
            'keywords' => explode(',', $this->keywords),
        ];
    }

}