<?php

/**
 * Model class for table "author"
 *
 * 'author' fields:
 * @property integer $id
 * @property string $name
 */
class Author extends CActiveRecord implements ApiInterface
{
    /**
     * @return Book the static model class
     */
    public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'author';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('name', 'length', 'max'=>255),
			array('id, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
		);
	}

	/**
     * @return array The fields exposed to the API
     */
    public function apiAttributes()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}