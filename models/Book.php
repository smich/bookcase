<?php

/**
 * Model class for table "book"
 *
 * 'book' fields:
 * @property integer $id
 * @property string $title
 * @property integer $author_id
 * @property integer $genre_id
 * @property string $cover
 * @property string $search_keywords
 */
class Book extends CActiveRecord implements ApiInterface
{
	/**
	 * @return Book the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'book';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['author_id, genre_id', 'numerical', 'integerOnly'=>true],
			['title', 'length', 'max'=>45],
			['cover', 'length', 'max'=>256],
			['search_keywords', 'safe'],
			['id, title, author_id, genre_id, cover, search_keywords', 'safe', 'on'=>'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
            'author' => [self::BELONGS_TO, 'Author', 'author_id', 'joinType' => 'INNER JOIN'],
            'genre' => [self::BELONGS_TO, 'Genre', 'genre_id', 'joinType' => 'INNER JOIN'],
            'chapters' => [self::HAS_MANY, 'Chapter', 'book_id'],
            'chaptersCount' => [self::STAT, 'Chapter', 'book_id', 'select' => 'count(t.id)'],
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Title',
			'author_id' => 'Author',
			'genre_id' => 'Genre',
			'cover' => 'Cover Img',
			'search_keywords' => 'Search Keywords',
		];
	}

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public static function search($attrs)
    {
        // Default ordering is by genre
        if (!isset($attrs['order'])) {
            $attrs['order'] = 'genre';
        }

        $criteria = new CDbCriteria;
        $criteria->alias = 'b';

        // Eager load needed relations
        $with = [
            'author' => [
                'select' => 'name',
                'alias' => 'a'
            ],
            'chapters' => [
                'select' => ['title', 'keywords'],
                'alias' => 'c'
            ],
            'genre' => [
                'select' => ['label', 'color'],
                'alias' => 'g'
            ]
        ];

        // Determine ordering
        switch($attrs['order'])
        {
            case 'author':
                $with['author']['order'] = 'a.name';
            break;
            case 'title':
                $criteria->order = 'b.title';
            break;
            default:
                $with['genre']['order'] = 'g.label';
            break;
        }

        if (isset($attrs['query'])) {
            $criteria->condition = "MATCH (search_keywords) AGAINST(:term IN BOOLEAN MODE)";
            $criteria->params = [
                ':term' => '+'.$attrs['query']
            ];
        }

        // NOTICE: Clearly fetching all is dangerous in case of bing DBs; happening only for demo purposes
        return Book::model()->with($with)->findAll($criteria);
    }

    /**
     * @return array The fields exposed to the API
     */
    public function apiAttributes()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'cover' => $this->cover,
        ];
    }
}