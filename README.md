##Project info:

***

### Techonologies

  - The server side built on top of yii framework. In order to install the framework run:

```sh
php composer.phar install
```

  This will crate the vendor folder and install the yii framework in it

  - The client side is backed up by the AngularJs framework
  
***

### Code structure
  - models/ - all model classes
  - controllers/ - all controller classes
  - views/ - view files/layouts
  - www/ - project resources (i.e css,imgs,js)

***

### Database

  - In your mysql client (command line or any tool you have available) run the following to create an empty db:

```sql
create database bookcase_db;
```
	
  - Via command line, go to the root of the project and run the following to create all of the DB tables required by the project:
  
```sh
./yiic migrate
```

  - If you want to load sample data run the following:
  
```sql
mysql -uroot -pqwerty bookcase_db < dump.sql
```

  - If you want to change the db credentials and not use the defaults (root:qwerty1) edit /config/auth.php

***

### Search service

 - As specified in the specs the full text search mechanism should search to any book fields including genre, author, title, chapter title or keyword. Due to normalization the query would try to match the terms against columns of different tables like, author.name, genre.label, chapter.keywords, chapter.title and book.title and the OR operator would have to be used which deteriorates performance significantly in case of big data sets. In order to work-around this issue, I opted good to create a text field in book table (i.e book.search_keywords) with a fulltext index on it that contains a comma concatenated list of all keywords that should be searchable.

 - The query that fetches the books is the following:

```sql
 SELECT *
 FROM `book` `b`
 INNER JOIN `author` `a` ON (`b`.`author_id`=`a`.`id`)
 LEFT OUTER JOIN `chapter` `c` ON (`c`.`book_id`=`b`.`id`)
 INNER JOIN `genre` `g` ON (`b`.`genre_id`=`g`.`id`)
 WHERE (MATCH (search_keywords) AGAINST(:term IN BOOLEAN MODE))
 ORDER BY a.name
```

 - Try to search using 4 letters or more. As I remember the default mysql configuration sets ft_min_word_len to 4

 - You can find the code related to searching in models/Book::search()

***

### UI

 - Try to resize the window to see a minimum responsive functionality

 - You can change the shelf capacity that is by default set to 5, as specified in the specs, in order to see how the books are placed in shelves that are created on demand. In order to do so change the SHELF_CAPACITY constant in www/js/app.js