<?php

$dir = dirname(__FILE__);
// change the following paths if necessary
$yii=$dir.'/../vendor/yiisoft/yii/framework/yii.php';

// The the main webapp config for this environment (COMMITTED)
$config=require($dir.'/../config/main.php');

// The the local webapp config for this environment (NOT-COMMITTED)
$localconf=require($dir.'/../config/local.php');
$authconf=require($dir.'/../config/auth.php');

require_once($yii);

// Merge all configs toghether in a specific ordering
$config=CMap::mergeArray($config,$localconf);
$config=CMap::mergeArray($config,$authconf);

Yii::createWebApplication($config)->run();

