(function (window, angular) {

    window.Bookcase = angular.module('Bookcase', [])

        .constant('SHELF_CAPACITY', 5)
    ;

    Bookcase
        .config([ '$locationProvider', function ($locationProvider) {
            $locationProvider.html5Mode(true);
        }])

        /**
         * Application main controller
         */
        .controller('AppCtrl', [ '$http', '$rootScope', '$scope', '$location', 'SHELF_CAPACITY', function($http, $rootScope, $scope, $location, SHELF_CAPACITY) {

            $scope.shelfCapacity = SHELF_CAPACITY;
            var searchParams = $location.search();
            if (searchParams.order) {
                $scope.order = searchParams.order;
            }
            if (searchParams.query) {
                $scope.query = searchParams.query;
            }
            /**
             * Fetch the book list
             */
            $scope.fetchBooks = function(order, query)
            {
                var searchAttrs = $location.search();
                var url = ['/site/books'],
                    attrs = [];

                // Use provided order
                if (order) {
                    attrs.push('order='+order);
                    $scope.order = order;
                    $location.search('order', order);
                }
                // Use order from url param, if provided
                else if (searchAttrs.order) {
                    attrs.push('order='+searchAttrs.order);
                    $scope.order = $location.search().order;
                }

                // Use provided query
                if (query) {
                    attrs.push('query='+query);
                    $scope.query = query;
                    $location.search('query', query);
                }
                // Use term from url param, if provided
                else if (searchAttrs.query) {
                    attrs.push('query='+searchAttrs.query);
                }

                if (attrs.length > 0) {
                    url += '?' + attrs.join('&');
                }
                $http.get(url).success(function(data, status, headers, config) {

                    // Find number of shelves
                    var shelfCount       = Math.ceil(data.length / SHELF_CAPACITY),
                        booksInLastShelf = data.length % SHELF_CAPACITY,
                        shelves = [];

                    for (var i=0; i<shelfCount; i++)
                    {
                        var booksForShelf = (i == shelfCount) ? booksInLastShelf : SHELF_CAPACITY,
                            books = data.splice(0, booksForShelf)
                            genres = {};

                        // Traverse list of books to get a unique list of genres

                        for (var j in books) {
                            if (!genres[books[j].genre.label]) {
                                genres[books[j].genre.label] = {
                                    label: books[j].genre.label,
                                    color: books[j].genre.color
                                };
                            }
                            // In the case of the last book, plae the popover on the left
                            books[j].popoverPlacement = (j == books.length - 1 && books.length > 1) ? 'left' : 'right';
                        }
                        shelves.push({
                            genres: genres,
                            books: books
                        });
                    }
                    $scope.shelves = shelves;
                });
            };
            /**
             * Reset search term
             */
            $scope.resetTerm = function(order, query)
            {
                $scope.query = '';
                $location.search('query', '');
                $scope.fetchBooks();
            };

            $scope.fetchBooks();

        }])

        /**
         * Popover to display book chapters
         */
        .directive('bcPopover', function ($compile,$templateCache) {
            return {
                restrict: "A",
                scope: {
                    'bcTitle': '@',
                    'bcAuthor': '@',
                    'bcChapters': '=',
                    'bcCover': '@',
                    'bcPlacement': '@'
                },
                link: function (scope, element, attrs) {
                    if (!scope.bcPlacement) {
                        scope.bcPlacement = 'right';
                    }

                    // Build popover content
                    var content = "<header>";
                    if (scope.bcCover) {
                        content += '<img src="/css/img/book-covers/'+scope.bcCover+'" class="img-responsive"/>';
                    }
                    content += '<div class="info"><span class="title">'+scope.bcTitle+'</span><span class="author">'+scope.bcAuthor+'</span></div>' +
                        '</header>' +
                        '<section>' +
                        '<label>Chapters</label>'
                    ;

                    if (scope.bcChapters && scope.bcChapters.length == 0) {
                        content += "No chapters available for this book";
                    }
                    else {
                        content += "<ol>";
                        for (var i in scope.bcChapters) {
                            content += "<li>" +
                                '<span class="title">'+scope.bcChapters[i].title + "</span>" +
                                '<span class="keywords">'+scope.bcChapters[i].keywords.join(',') + "</span>" +
                                "</li>";
                        }
                        content += "</ul>";
                    }
                    content += '</section>';

                    var options = {
                        content: content,
                        delay: 500,
                        placement: scope.bcPlacement,
                        trigger: "hover",
                        html: true
                    };
                    $(element).popover(options);
                }
            };
        });
    ;

})(window, angular);
