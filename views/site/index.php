<div class="bookcase gutter-top">
    <!-- No Results found -->
    <div class="alert alert-warning" ng-if="shelves.length == 0">
        <span ng-if="query!=''">No books available matching your search term: <strong>"{{query}}"</strong></span>
        <span ng-if="query==''">There are no books available at the moment</span>
    </div>
    <!-- Build shelves -->
    <section class="shelf" ng-repeat="shelf in shelves" ng-if="shelves.length > 0">
        <header class="clearfix gutter-bottom">
            <ul>
                <li ng-repeat="genre in shelf.genres" style="background-color: {{genre.color}};">{{genre.label}}</li>
            </ul>
        </header>
        <section class="clearfix">
            <ul>
                <li ng-repeat="book in shelf.books" class="clearfix" style="width: {{100/shelfCapacity}}%;">
                    <div class="book">
                        <div class="cover" bc-popover bc-title="{{book.title}}" bc-author="{{book.author.name}}" bc-cover="{{book.cover}}" bc-chapters="book.chapters" bc-placement="{{book.popoverPlacement}}">
                            <img ng-if="book.cover" ng-src="/css/img/book-covers/{{book.cover}}" class="img-responsive">
                            <i ng-if="!book.cover" class="glyphicon glyphicon-picture"></i>
                            <div class="genre-indicator" style="border-bottom-color: {{book.genre.color}};"></div>
                        </div>
                        <div class="info gutter-top">
                            <span class="title">{{book.title}}</span>
                            <span class="author">{{book.author.name}}</span>
                        </div>
                    </div>
                </li>
            </ul>
        </section>
    </section>

</div>