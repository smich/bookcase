<?php
    $menuItems = array(
        array(
            'label'      => '<i class="glyphicon glyphicon-tag">&nbsp;</i>'.'Genre',
            'class'      => "{'active': order=='genre' || !order}",
            'onClick'    => "fetchBooks('genre')",
            'url'        => '?order=genre',
        ),
        array(
            'label'      => '<i class="glyphicon glyphicon-user">&nbsp;</i>'.'Author',
            'class'      => "{'active': order=='author'}",
            'onClick'    => "fetchBooks('author')",
            'url'        => '?order=author',
        ),
        array(
            'label'      => '<i class="glyphicon glyphicon-book">&nbsp;</i>'.'Title',
            'class'      => "{'active': order=='title'}",
            'onClick'    => "fetchBooks('title')",
            'url'        => '?order=title',
        )
    );
?>

<div class="myheader clearfix">
    <div class="inner clearfix">
        <h3 class="myheader-brand">BookCase</h3>
        <ul class="nav myheader-nav">
            <li class="search-form-container">
                <form name="form" role="form" class="search-form">
                    <input type="text" ng-model="query"/>
                    <button ng-click="fetchBooks('genre',query);"><i class="glyphicon glyphicon-search"></i></button>
                    <button ng-click="resetTerm();"><i class="glyphicon glyphicon-remove"></i></button>
                </form>
            </li>
            <li><label>Order by:</label></li>
            <?php
            foreach($menuItems as $mi)
            {
                echo CHtml::tag('li', array('ng-class' => $mi['class']),
                    CHtml::tag('a', array(
                        'href'=>$mi['url'],
                        'ng-click' => $mi['onClick']
                    ), $mi['label'])
                );
            }
            ?>
        </ul>
    </div>
</div>