<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="language" content="en" />
    <meta name="author" content="smich" />
    <meta name="robots" content="index, follow" />

    <title>
    <?php
        $pageTitle =  ($this->pageTitle != Yii::app()->name) ? $this->pageTitle . " - " . Yii::app()->name : $this->pageTitle;
        echo CHtml::encode($pageTitle);
    ?>
    </title>
    <link rel="icon" type="image/ico" href="/favicon.ico"></link>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/main.css">

    <?php
        $cs = Yii::app()->clientScript;
    ?>
    <script type="text/javascript">
    </script>
</head>

<body class="<?php echo Yii::app()->controller->id .' '. $this->getAction()->getId(); ?>" ng-app="Bookcase" ng-controller="AppCtrl">

    <div class="site-wrapper">

        <div class="site-wrapper-inner">

            <?php
                $this->renderPartial('//layouts/_topnav');
            ?>

            <div class="content-container">

                <?php
                    echo $content;
                ?>

            </div>

        </div>

    </div>

    <script src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.9/angular.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <script src="/js/app.js"></script>

</body>
</html>
