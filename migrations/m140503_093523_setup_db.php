<?php

class m140503_093523_setup_db extends CDbMigration
{
    /**
     * Sets up the DB
     * @return bool|void
     */
    public function up()
	{
        // Set the db encoding to utf8
        $this->execute("ALTER SCHEMA `bookcase_db` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;");
        // Create the DB structure...

        // author
        if (!isset(Yii::app()->db->schema->tables['author'])) {
            $this->execute("
                CREATE TABLE `author` (
                  `id` INT NOT NULL AUTO_INCREMENT,
                  `name` VARCHAR(255) NULL,
                  PRIMARY KEY (`id`),
                  INDEX `idx_name` (`name` ASC))
                  DEFAULT CHARACTER SET = utf8
                  COLLATE = utf8_general_ci;
            ");
        }
        // book
        if (!isset(Yii::app()->db->schema->tables['book'])) {
            $this->execute("
                CREATE TABLE `book` (
                  `id` INT NOT NULL AUTO_INCREMENT,
                  `title` VARCHAR(45) NULL,
                  `author_id` INT NULL,
                  `genre_id` INT NULL,
                  `cover` VARCHAR(256) NULL,
                  `search_keywords` TEXT NULL COMMENT 'Contains all searchable information imploded by comma',
                  PRIMARY KEY (`id`),
                  FULLTEXT INDEX `search_keywords_idx` (`search_keywords` ASC),
                  INDEX `idx_title` (`title` ASC))
                  ENGINE = MyISAM
                  DEFAULT CHARACTER SET = utf8
                  COLLATE = utf8_general_ci;
            ");
        }
        // chapter
        if (!isset(Yii::app()->db->schema->tables['chapter'])) {
            $this->execute("
                CREATE TABLE `chapter` (
                  `id` INT NOT NULL AUTO_INCREMENT,
                  `title` VARCHAR(512) NULL,
                  `keywords` TEXT NULL COMMENT 'Comma concatenating list of keywords/phrases',
                  `book_id` INT NULL,
                  PRIMARY KEY (`id`))
                  DEFAULT CHARACTER SET = utf8
                  COLLATE = utf8_general_ci;
            ");
        }
        // genre
        if (!isset(Yii::app()->db->schema->tables['genre'])) {
            $this->execute("
                CREATE TABLE `genre` (
                  `id` INT NOT NULL AUTO_INCREMENT,
                  `label` VARCHAR(45) NULL,
                  `color` VARCHAR(7) NULL,
                  PRIMARY KEY (`id`),
                  INDEX `idx_label` (`label` ASC))
                  DEFAULT CHARACTER SET = utf8
                  COLLATE = utf8_general_ci;
            ");
        }
	}

    /**
     * Reverts the changes applied via the up method
     * @return bool
     */
    public function down()
	{
        // Drop the tables
        if (isset(Yii::app()->db->schema->tables['author'])) {
            $this->dropTable('author');
        }
        if (isset(Yii::app()->db->schema->tables['book'])) {
            $this->dropTable('book');
        }
        if (isset(Yii::app()->db->schema->tables['chapter'])) {
            $this->dropTable('chapter');
        }
        if (isset(Yii::app()->db->schema->tables['genre'])) {
            $this->dropTable('genre');
        }

		return true;
	}
}