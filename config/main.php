  <?php

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'BookCase',
    //'language' => 'el',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'qwerty1',
            'ipFilters' => array('*'),
            'generatorPaths' => array(
                'application.extensions.gii',
            ),
        ),
        'clientManager' => array(),
        'admin' => array(),
    ),
    // application components
    'components' => array(
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'showScriptName'=> false,         // Don't show entry script name in the constructed URL (index.php).
            // 'appendParams'  => false,         // false to use '&' to separate the GET params, true to use '/'. Always in combination with urlFormat = path
            'urlFormat'     => 'path',        // path format is more humand readable: /path/to/EntryScript.php/name1/value1/name2/value2
            'rules' => array(
                '/' => '/site/index',
                'site/books/search/<query:.*>/order/<order:.*>' => 'site/books',

                'gii' => 'gii',
                'gii/<controller:\w+>' => 'gii/<controller>',
                'gii/<controller:\w+>/<action:\w+>' => 'gii/<controller>/<action>',

                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        'log' => array(
            'class' => 'CLogRouter',
        ),
    ),
    // Application-level parameters - accessed via Yii::app()->params['paramName']
    'params' => array(
    ),
);
