<?php

return array(
    'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=bookcase_db',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'qwerty1',
            'charset' => 'utf8',
        ),
    )
);
